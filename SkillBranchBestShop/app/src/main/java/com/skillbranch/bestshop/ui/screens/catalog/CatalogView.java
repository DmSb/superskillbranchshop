package com.skillbranch.bestshop.ui.screens.catalog;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.Button;

import com.skillbranch.bestshop.R;
import com.skillbranch.bestshop.di.DaggerService;
import com.skillbranch.bestshop.mvp.views.AbstractView;
import com.skillbranch.bestshop.mvp.views.ICatalogView;
import com.skillbranch.bestshop.ui.screens.product.ProductView;

import butterknife.BindView;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class CatalogView extends AbstractView<CatalogScreen.CatalogPresenter> implements ICatalogView {
    private static final String TAG = "CatalogView";

//    @Inject
//    CatalogScreen.CatalogPresenter mPresenter;

    @BindView(R.id.add_to_card_btn)
    Button mAddToCardBtn;
    @BindView(R.id.product_pager)
    ViewPager mProductPager;
    @BindView(R.id.indicator)
    CircleIndicator mIndicator;

    private CatalogAdapter mAdapter;

    public CatalogView(Context context, AttributeSet attrs) {
        super(context, attrs);
//        if(!isInEditMode()) {
//            DaggerService.<CatalogScreen.Component>getDaggerComponent(context).inject(this);
//        }
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<CatalogScreen.Component>getDaggerComponent(context).inject(this);
        mAdapter = new CatalogAdapter();
    }

    @Override
    public void showCatalogView() {
        mProductPager.setAdapter(mAdapter);
        mIndicator.setViewPager(mProductPager);
        mAdapter.registerDataSetObserver(mIndicator.getDataSetObserver());
    }

    @Override
    public void updateProductCounter() {

    }

    public int getCurrentPagerPosition() {
        return mProductPager.getCurrentItem();
    }

    @Override
    public boolean viewOnBackPressed() {
        return getCurrentProductView().viewOnBackPressed();
    }

    @OnClick(R.id.add_to_card_btn)
     void clickAddToCard() {
        mPresenter.clickOnBuyButton(mProductPager.getCurrentItem());
    }

    public CatalogAdapter getAdapter() {
        return mAdapter;
    }

    public ProductView getCurrentProductView() {
        return (ProductView) mProductPager.findViewWithTag("Product"+ mProductPager.getCurrentItem());
    }
}
