package com.skillbranch.bestshop.mvp.presenters;

public interface IProductPresenter {
    public void clickOnMinus();
    public void clickOnPlus();
}
